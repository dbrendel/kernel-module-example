#include <linux/module.h>
#include <linux/printk.h>

MODULE_LICENSE("GPL");

static int __init my_main(void) {
    printk(KERN_ERR "Hello QE!!\n");

    return 0;
}

static void __exit my_exit(void) {
    printk(KERN_ERR "Bye QE!!\n");
}

module_init(my_main);
module_exit(my_exit);

